#-*- coding: utf-8 -*-

from django import forms
from note_user.models import NoteUser
from django.contrib.auth import authenticate

def UniqueEmailValidator(value):
    if NoteUser.objects.filter(email=value).exists():
        raise forms.ValidationError(u'该邮箱已经被注册')

class RegisterForm(forms.ModelForm):
    '''要保存数据到数据库用forms.ModelForm,只是验证的话用forms.Form'''
    email = forms.CharField(widget=forms.EmailInput(attrs={'class': 'form-control',
                                                           'style': 'background-color: transparent; color: white; border: None; border-bottom: 1px solid #ffffff; width: 100%;'}),
                            required=True,
                            label=u'邮箱',
                            error_messages={'required':u'请输入邮箱','invalid':u'请输入有效邮箱'},
                            help_text=u'请输入有效的邮箱')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                 'style': 'background-color: transparent; color: white; border: None; border-bottom: 1px solid #ffffff; width: 100%;'}),
                               label=u"密码",
                               required=True)
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                         'style': 'background-color: transparent; color: white; border: None; border-bottom: 1px solid #ffffff; width: 100%;'}),
                                       label=u"确认密码",
                                       required=True)

    class Meta:
        model = NoteUser
        fields = ['email', 'password', 'confirm_password']

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.fields['email'].validators.append(UniqueEmailValidator)

    def clean(self):
        super(RegisterForm, self).clean()
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if len(password) < 6:
            self._errors['password'] = self.error_class([u'密码至少6位'])
        if password and password != confirm_password:
            self._errors['confirm_password'] = self.error_class([u'两次密码不匹配'])
        return self.cleaned_data

class LoginForm(forms.Form):
    '''
    要保存数据到数据库用forms.ModelForm,只是验证的话用forms.Form
    如果这里用forms.Model每次登录由于email是unique每次都会验证email是否存在
    这样会出现email already exist 的error
    '''
    email = forms.CharField(widget=forms.EmailInput(attrs={'class': 'form-control', 'style': 'border:none; border-bottom: 1px solid #ffffff;color: white; background-color: transparent; outline: none;'}),
                            required=True,
                            label=u'邮箱')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'style': 'border:none; border-bottom: 1px solid #ffffff;color: white; background-color: transparent; outline: none;'}),
                               label=u"密码",
                               required=True)

    def clean(self):
        super(LoginForm, self).clean()
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if email and password:
            self.user_cache = authenticate(email=email, password=password)
            if self.user_cache is None:
                self._errors['password'] = self.error_class([u'密码错误'])
            elif not self.user_cache.is_active:
                self._errors['email'] = self.error_class([u'此账号已被禁用'])
        return self.cleaned_data

    def get_user(self):
        return self.user_cache

class ChangePasswordForm(forms.ModelForm):
    id = forms.CharField(widget=forms.HiddenInput())
    old_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                   label=u'旧密码',
                                   required=True)
    new_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                   label=u'新密码',
                                   required=True)
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                   label=u'确认新密码',
                                   required=True)

    class Meta:
        model = NoteUser
        fields = ['id', 'old_password', 'new_password', 'confirm_password']

    def clean(self):
        super(ChangePasswordForm, self).clean()
        old_password = self.cleaned_data.get('old_password')
        new_password = self.cleaned_data.get('new_password')
        confirm_password = self.cleaned_data.get('confirm_password')
        id = self.cleaned_data.get('id')
        user = NoteUser.objects.get(pk=id)
        if not user.check_password(old_password):
            self._errors['old_password'] = self.error_class([u'旧密码错误'])
        if new_password != confirm_password:
            self._errors['confirm_password'] = self.error_class([u'密码不匹配'])
        return self.cleaned_data