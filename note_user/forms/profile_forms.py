#-*- coding: utf-8 -*-

from django import forms
from ..models import UserProfile

class ProfileForm(forms.ModelForm):
    SEXUAL_CHOICE = ((u'0', u'女'), (u'1', u'男'))
    nickname = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                               label=u'昵称',
                               max_length=30,
                               required=False)
    sexual = forms.IntegerField(widget=forms.RadioSelect(choices=SEXUAL_CHOICE, attrs={'class': ' radio-inline'}),
                                label=u'性别',
                                required=False)
    job = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                               label=u'职业',
                               max_length=30,
                               required=False)
    url = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                               label=u'个人站点',
                               max_length=128,
                               required=False)
    describe = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                          label=u'个人标签',
                          max_length=128,
                          required=False)
    headshot = forms.ImageField(label=u'头像', required=False)
    '''用ImageField或者FileField在html的form标签中定义enctype="multipart/form-data"'''

    class Meta:
        model = UserProfile
        fields=['headshot', 'nickname', 'sexual', 'job', 'url', 'describe']

    def clean(self):
        super(ProfileForm, self).clean()
        nickname = self.cleaned_data.get('nickname')
        sexual = self.cleaned_data.get('sexual')
        job = self.cleaned_data.get('job')
        url = self.cleaned_data.get('url')
        describe = self.cleaned_data.get('describe')
        headshot = self.cleaned_data.get('headshot')
        return self.cleaned_data