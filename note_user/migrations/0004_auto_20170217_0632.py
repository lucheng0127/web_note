# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-17 06:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('note_user', '0003_auto_20170217_0604'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='sexual',
            field=models.IntegerField(default=1),
        ),
    ]
