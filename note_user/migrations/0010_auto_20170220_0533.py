# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-20 05:33
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('note_user', '0009_auto_20170220_0343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noteuser',
            name='following',
            field=models.ManyToManyField(related_name='_noteuser_following_+', to=settings.AUTH_USER_MODEL),
        ),
    ]
