#-*- coding: utf-8 -*-

import json
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from note_user.forms.user_forms import RegisterForm, LoginForm, ChangePasswordForm
from utils.utils import render_ack
from note_user.models import NoteUser, UserProfile, UserRelationship


def register(request):
    if request.method == 'POST':
        # data = json.loads(request.body)
        # email = data['email']
        # password = data['password']
        # confirm_password = data['confirm_password']
        # NoteUser.objects.create_user(email=email, password=password)
        # user = authenticate(email=email, password=password)
        # login(request, user)
        # profile = UserProfile(user=user)
        # profile.save()
        # ur = UserRelationship(user=user)
        # ur.save()
        # return render_ack(status='success', msg='注册成功')
        form = RegisterForm(request.POST)
        if not form.is_valid():
            return render(request, 'note_user/register.html', {'form': form})
        else:
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            NoteUser.objects.create_user(email=email, password=password)
            user = authenticate(email=email, password=password)
            login(request, user)
            profile = UserProfile(user=user)
            profile.save()
            ur = UserRelationship(user=user)
            ur.save()
            return redirect(reverse('setProfile'))
    else:
        return render(request, 'note_user/register.html', {'form': RegisterForm()})

def userLogin(request):
    if request.user.is_authenticated():
        return render(request, 'web_note/index.html')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return render(request, 'web_note/index.html', {'user': user})
        else:
            return render(request, 'note_user/login.html', {'form': form})
    else:
        return render(request, 'note_user/login.html', {'form': LoginForm()})

'''自定义的login换个名字并在setting中定义LOGIN_URL'''
@login_required
def userLogout(request):
    logout(request)
    return redirect(reverse('homepage'))

@login_required
def ChangePassword(request):
    user = request.user
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            new_password = form.cleaned_data.get('new_password')
            user.set_password(new_password)
            user.save()
            user = authenticate(email=user.email, password=new_password)
            login(request, user)
            messages.add_message(request, messages.SUCCESS, u'密码修改成功')
    else:
        form = ChangePasswordForm(instance=user)
    return render(request, 'note_user/changePassword.html', {'form': form})

def url_test(request, email):
    # email = request.GET.get('email')
    print email
    return render(request, 'web_note/test.html', {'email': email})

# def test(request):
#     '''用户follow部分查询实验代码'''
#     user1 = NoteUser.objects.get(pk=11)
#     user2 = NoteUser.objects.get(pk=12)
#     print u'用户'
#     print user1
#     print user2
#     ur1 = user1.ur
#     ur2 = user2.ur
#     user1_name = UserRelationship.objects.get(pk=1)
#     user2_name = UserRelationship.objects.get(pk=2)
#     print u'用户名'
#     print user1_name.user
#     print ur2.user.email
#     ur2.following.add(user1)
#     ur2.save()
#     for item in ur2.following.all():
#         print item.email
#     for item in user1.followers.all():
#         print item.user.profile.sexual
#     return render(request, 'web_note/homePage.html')