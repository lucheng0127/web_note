#-*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from ..forms.profile_forms import ProfileForm

@login_required
def setProfile(request):
    user = request.user
    profile = user.profile
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            profile.nickname = form.cleaned_data.get('nickname')
            profile.sexual = form.cleaned_data.get('sexual')
            profile.job = form.cleaned_data.get('job')
            profile.url = form.cleaned_data.get('url')
            profile.describe = form.cleaned_data.get('describe')
            if form.cleaned_data.get('headshot'):
                profile.headshot = form.cleaned_data.get('headshot')
            profile.save()
            messages.add_message(request, messages.SUCCESS, u'个人资料编辑成功')
            return redirect(reverse('homepage'))
    else:
        form = ProfileForm(instance=profile, initial={
            'nickname': profile.nickname,
            'sexual': profile.sexual,
            'job': profile.job,
            'url': profile.url,
            'describe': profile.describe,
            'headshot': profile.headshot
        })
        return render(request, 'note_user/profile/profile.html', {'form': form})
