#-*- coding: utf-8 -*-

from django.conf.urls import url
from django.urls import reverse
from .views import user, profile

urlpatterns = [
    url(r'^register/$', user.register, name='register'),
    url(r'^login/$', user.userLogin, name='login'),
    url(r'^logout/$', user.userLogout, name='logout'),
    url(r'^changePassword/$', user.ChangePassword, name='changePassword'),
    url(r'^profile/$', profile.setProfile, name='setProfile'),
    url(r'^url_test/(?P<email>[^/]+)/$', user.url_test, name='url_test'),
    # url(r'^test/$', user.test, name='test'),
]