#-*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from .noteUserBackends import NoteUserManager

class NoteUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name='Email address',
        max_length=255,
        unique=True,
    )
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = NoteUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.email

    '''has_module_perms为True则user可以登录Django的Admin后台'''
    # def has_module_perms(self, app_label):
    #     return True

    @property
    def is_staff(self):
        return self.is_admin

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

class UserProfile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name='profile',
        on_delete=models.CASCADE,
    )
    nickname = models.CharField(max_length=30)
    sexual = models.IntegerField(default=1)
    job = models.CharField(max_length=30, null=True)
    url = models.CharField(max_length=128, null=True)
    describe = models.CharField(max_length=128, null=True)
    headshot = models.ImageField(upload_to='headshots', null=True)

class UserRelationship(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name='ur',
        on_delete=models.CASCADE,
    )
    following = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='followers'
    )
