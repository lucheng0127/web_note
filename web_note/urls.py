#-*- coding: utf-8 -*-
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings
admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name='web_note/homePage.html'), name='homepage'),
    url(r'^', include('note_user.urls')),
    url(r'^', include('note.urls')),
    url(r'^', include('snippets.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
'''访问media中的文件需要在url中加上如下：
 + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
'''