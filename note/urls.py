#-*- coding: utf-8 -*-

from django.conf.urls import url
from forms.note_forms import NoteForm
from .views import note_views

urlpatterns = [
    url(r'^newNote/$', note_views.newNote, name='newNote'),
]