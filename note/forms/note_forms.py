#-*- coding: utf-8 -*-

from django import forms
from ..models import Note

class NoteForm(forms.ModelForm):
    IS_PUBLIC_CHOICE = ((True, u'公开'), (False, u'私密'))
    DOWNLOAD_CHOICE = ((True, u'可下载'), (False, u'只读'))
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form form-control'}),
                            label=u'标题',
                            required=True,
                            error_messages={'require': u'笔记一定要有标题'})
    content = forms.CharField(widget=forms.Textarea(attrs={'class': 'form form-control'}),
                              required=False,
                              label=u'正文')
    is_public = forms.BooleanField(widget=forms.RadioSelect(choices=IS_PUBLIC_CHOICE),
                                   required=False)
    can_download = forms.BooleanField(required=False,
                                      widget=forms.RadioSelect(choices=DOWNLOAD_CHOICE))

    class Meta:
        model = Note
        fields = ['title', 'content', 'is_public', 'can_download']

    def clean(self):
        super(NoteForm, self).clean()
        title = self.cleaned_data.get('title')
        content = self.cleaned_data.get('content')
        is_public = self.cleaned_data.get('is_public')
        can_download = self.cleaned_data.get('can_download')
        return self.cleaned_data