#-*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.conf import settings

class Note(models.Model):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='note',
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=40)
    content = models.TextField(null=True)
    create_date = models.DateTimeField(auto_now_add=True) #model创建的时间
    update_date = models.DateTimeField(auto_now=True) #model每次修改的时间
    is_public = models.BooleanField(default=False)
    can_download = models.BooleanField(default=False)
