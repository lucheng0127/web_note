#-*- coding: utf-8 -*-

import json
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from ..models import Note
from ..forms.note_forms import NoteForm

@login_required
def newNote(request):
    user = request.user
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if not form.is_valid():
            return render(request, 'note/newNote.html', {'form': form})
        else:
            title = form.cleaned_data.get('title')
            content = form.cleaned_data.get('content')
            is_public =False
            if form.cleaned_data.get('is_public'):
                is_public = True
            can_download = False
            if form.cleaned_data.get('can_download'):
                can_download = True
            note = Note(author=user, title=title, content=content, is_public=is_public, can_download=can_download)
            note.save()
            form = NoteForm(instance=note, initial={
                'title': note.title,
                'content': note.content,
                'is_public': note.is_public,
                'can_download': note.can_download
            })
            data = {
                'note_id': note.id
            }
            return render(request, 'note/newNote.html', {'note': note, 'form': form, 'note_id': note.id})
    else:
        return render(request, 'note/newNote.html', {'form': NoteForm()})