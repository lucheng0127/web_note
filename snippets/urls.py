#-*- coding: utf-8 -*-
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from snippets.views import  snippets_views

urlpatterns = [
    url(r'snippets/$', snippets_views.SnippetList.as_view(), name='snippet_list'),
    url(r'snippets/(?P<pk>[0-9]+)/$', snippets_views.SnippetDetail.as_view(), name='snippet_detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)