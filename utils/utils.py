#-*- coding: utf-8 -*-

import json
from django.http import HttpResponse

def render_ack(status='success', msg='', data={}):
    respone_data = {}
    respone_data['status'] = status
    respone_data['msg'] = msg
    respone_data['data'] = data
    return HttpResponse(json.dumps(respone_data),
                        content_type="application/json")